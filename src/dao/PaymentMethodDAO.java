package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import connection.ConnectionFactory;
import model.PaymentMethod;
import model.Product;

public class PaymentMethodDAO
{

	public static PaymentMethod findById(int searchedId)
	{
		PaymentMethod paymentMethod = null;
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		ResultSet resultSet = null;
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("SELECT * FROM paymentmethod WHERE id = ?");
			preparedStatement.setLong(1, searchedId);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			
			String method = resultSet.getString("method");
			paymentMethod = new PaymentMethod(searchedId, method);
			
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
		ConnectionFactory.close(resultSet);
		return paymentMethod;
	}
	
	public static void delete(int id)
	{
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("DELETE FROM paymentmethod WHERE id = ?");
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
	}
	
	public static void add(PaymentMethod paymentMethod)
	{
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("INSERT INTO paymentmethod (id, method) VALUES (?, ?)");
			preparedStatement.setLong(1, paymentMethod.getId());
			preparedStatement.setString(2, paymentMethod.getPaymentMethod());
			
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
	}
	
	public static ArrayList<PaymentMethod> selectAll()
	{
		ArrayList<PaymentMethod> allPaymentMethods = new ArrayList<PaymentMethod>();
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		ResultSet resultSet = null;
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("SELECT * FROM paymentmethod");
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next() == true)
			{
				int id = resultSet.getInt("id");
				String method = resultSet.getString("method");
				PaymentMethod pm = new PaymentMethod(id, method);
				allPaymentMethods.add(pm);
			}
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
		ConnectionFactory.close(resultSet);
		return allPaymentMethods;
	}
	
	public static void update(PaymentMethod paymentMethod)
	{
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("UPDATE paymentmethod SET method = ? WHERE id = ?");
			preparedStatement.setString(1, paymentMethod.getPaymentMethod());
			preparedStatement.setLong(2, paymentMethod.getId());
			
			
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
	}

	
	
	
}
