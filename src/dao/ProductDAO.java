package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import connection.ConnectionFactory;
import model.Product;

public class ProductDAO
{

	public static Product findById(int searchedId)
	{
		Product product = null;
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		ResultSet resultSet = null;
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("SELECT * FROM product WHERE id = ?");
			preparedStatement.setLong(1, searchedId);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			
			String name = resultSet.getString("name");
			int cost = resultSet.getInt("cost");
			int stock = resultSet.getInt("stock");
			product = new Product(searchedId, name, cost, stock);
			
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
		ConnectionFactory.close(resultSet);
		return product;
	}
	
	public static ArrayList<Product> selectAll()
	{
		ArrayList<Product> allProducts = new ArrayList<Product>();
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		ResultSet resultSet = null;
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("SELECT * FROM product");
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next() == true)
			{
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				int cost = resultSet.getInt("cost");
				int stock = resultSet.getInt("stock");
				Product product = new Product(id, name, cost, stock);
				allProducts.add(product);
			}
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
		ConnectionFactory.close(resultSet);
		return allProducts;
	}
	
	public static void delete(int id)
	{
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("DELETE FROM product WHERE id = ?");
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
	}
	
	public static void add(Product productToBeAdded)
	{
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("INSERT INTO product (id, name, cost, stock) VALUES (?, ?, ?, ?)");
			preparedStatement.setLong(1, productToBeAdded.getId());
			preparedStatement.setString(2, productToBeAdded.getName());
			preparedStatement.setLong(3, productToBeAdded.getCost());
			preparedStatement.setInt(4, productToBeAdded.getStock());
			
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
	}
	
	public static void update(Product productToBeUpdated)
	{
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("UPDATE product SET name = ?, cost = ?, stock = ? WHERE id = ?");
			preparedStatement.setString(1, productToBeUpdated.getName());
			preparedStatement.setLong(2, productToBeUpdated.getCost());
			preparedStatement.setLong(3, productToBeUpdated.getStock());
			preparedStatement.setLong(4, productToBeUpdated.getId());
			
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
	}
}
