package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import connection.ConnectionFactory;
import model.Client;

public class ClientDAO
{
	public static Client findById(int searchedId)
	{
		Client client = null;
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		ResultSet resultSet = null;
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("SELECT * FROM client WHERE id = ?");
			preparedStatement.setLong(1, searchedId);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			
			String name = resultSet.getString("name");
			int money = resultSet.getInt("money");
			String email = resultSet.getString("email");
			client = new Client(searchedId, name, money, email);
			
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
		ConnectionFactory.close(resultSet);
		return client;
	}
	
	public static ArrayList<Client> selectAll()
	{
		ArrayList<Client> allClients = new ArrayList<Client>();
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		ResultSet resultSet = null;
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("SELECT * FROM client");
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next() == true)
			{
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				int money = resultSet.getInt("money");
				String email = resultSet.getString("email");
				Client client = new Client(id, name, money, email);
				allClients.add(client);
			}
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
		ConnectionFactory.close(resultSet);
		return allClients;
	}
	
	public static void delete(Client clientToBeDeleted)
	{
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("DELETE FROM client WHERE id = ?");
			preparedStatement.setLong(1, clientToBeDeleted.getId());
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
	}
	
	public static void delete(int id)
	{
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("DELETE FROM client WHERE id = ?");
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
	}
	
	public static void add(Client clientToBeAdded)
	{
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("INSERT INTO client (id, name, money, email) VALUES (?, ?, ?, ?)");
			preparedStatement.setLong(1, clientToBeAdded.getId());
			preparedStatement.setString(2, clientToBeAdded.getName());
			preparedStatement.setLong(3, clientToBeAdded.getMoney());
			preparedStatement.setString(4, clientToBeAdded.getEmail());
			
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
	}
	
	
	public static void update(Client clientToBeUpdated)
	{
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("UPDATE client SET name = ?, money = ?, email = ? WHERE id = ?");
			preparedStatement.setString(1, clientToBeUpdated.getName());
			preparedStatement.setLong(2, clientToBeUpdated.getMoney());
			preparedStatement.setString(3, clientToBeUpdated.getEmail());
			preparedStatement.setLong(4, clientToBeUpdated.getId());
			
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
	}
	
}
