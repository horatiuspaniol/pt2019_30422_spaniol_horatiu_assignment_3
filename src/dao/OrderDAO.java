package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.Client;
import model.Order;
import model.PaymentMethod;
import model.Product;

public class OrderDAO
{
	public static Order findById(int searchedId)
	{
		Order order = null;
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		ResultSet resultSet = null;
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("SELECT * FROM warehouse.`order` WHERE id = ?");
			preparedStatement.setLong(1, searchedId);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			
			int clientId = resultSet.getInt("clientid");
			int productId = resultSet.getInt("productid");
			int paymentId = resultSet.getInt("paymentid");
			int quantity = resultSet.getInt("quantity");
			order = new Order(searchedId, clientId, productId, paymentId, quantity);
			
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
		ConnectionFactory.close(resultSet);
		return order;
	}
	
	public static ArrayList<Order> selectAll()
	{
		ArrayList<Order> allOrders = new ArrayList<Order>();
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		ResultSet resultSet = null;
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("SELECT * FROM warehouse.`order`");
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next() == true)
			{
				int id = resultSet.getInt("id");
				int clientId = resultSet.getInt("clientid");
				int productId = resultSet.getInt("productid");
				int paymentId = resultSet.getInt("paymentid");
				int quantity = resultSet.getInt("quantity");
				Order order = new Order(id, clientId, productId, paymentId, quantity);
				allOrders.add(order);
			}
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
		ConnectionFactory.close(resultSet);
		return allOrders;
	}
	
	public static void delete(int id)
	{
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("DELETE FROM warehouse.`order` WHERE id = ?");
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
	}
		

	public static void add(Client client, Product product, PaymentMethod paymentMethod, int quantity)
	{
		if(isEnoughStock(product,quantity) == true)
		{
			reduceStock(product,quantity);
			Order order = null;
			PreparedStatement preparedStatement = null;
			Connection dbConnection = ConnectionFactory.getConnection();
			
			try
			{
				preparedStatement = dbConnection.prepareStatement("INSERT INTO warehouse.`order` (clientid, productid, paymentid, quantity) VALUES (?, ?, ?, ?)");
				preparedStatement.setLong(1, client.getId());
				preparedStatement.setLong(2, product.getId());
				preparedStatement.setLong(3, paymentMethod.getId());
				preparedStatement.setLong(4, quantity);
				
				preparedStatement.executeUpdate();
			} 
			catch (SQLException e)
			{
				e.printStackTrace();
			}
	
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(preparedStatement);
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Not enough stock!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public static void update(Order orderToBeUpdated)
	{
		PreparedStatement preparedStatement = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		
		try
		{
			preparedStatement = dbConnection.prepareStatement("UPDATE warehouse.`order` SET clientid = ?, productid = ?,  paymentid = ? WHERE id = ?");
			preparedStatement.setLong(1, orderToBeUpdated.getClientId());
			preparedStatement.setLong(2, orderToBeUpdated.getProductId());
			preparedStatement.setLong(3, orderToBeUpdated.getPaymentId());
			preparedStatement.setLong(4, orderToBeUpdated.getId());
			
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(preparedStatement);
	}
	
	private static void reduceStock(Product productFromOrderTable, int quantity)
	{
		Product productFromProductTable = ProductDAO.findById(productFromOrderTable.getId());
		productFromProductTable.setStock(productFromProductTable.getStock() - quantity);
		ProductDAO.update(productFromProductTable);
	}
	
	private static boolean isEnoughStock(Product productFromOrderTable, int quantity)
	{
		Product productFromProductTable = ProductDAO.findById(productFromOrderTable.getId());
		if(productFromProductTable.getStock() >= quantity)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
}
