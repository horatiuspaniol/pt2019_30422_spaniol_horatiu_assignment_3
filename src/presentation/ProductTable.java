package presentation;

import java.awt.Color;
import java.awt.Font;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Product;

public class ProductTable extends JTable
{
	static DefaultTableModel model;
	
	ProductTable() throws IllegalArgumentException, IllegalAccessException
	{
        Product product = new Product();
        Vector<String> columns = View.getFieldsOfObject(product);
        
        model = new DefaultTableModel();
        model.setColumnIdentifiers(columns);
        this.setModel(model);
        
        this.setBackground(Color.LIGHT_GRAY);
        this.setForeground(Color.black);
        Font font = new Font("",1,22);
        this.setFont(font);
        this.setRowHeight(30);
	}

	public void addProduct(Product product)
	{
		Object[] row = new Object[4];
		row[0] = product.getId();
		row[1] = product.getName();
		row[2] = product.getCost();
		row[3] = product.getStock();
		model.addRow(row);	
	}
}
