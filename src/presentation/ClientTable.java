package presentation;
import java.awt.Color;
import java.awt.Font;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Client;

public class ClientTable extends JTable
{
	static DefaultTableModel model;
	
	ClientTable() throws IllegalArgumentException, IllegalAccessException
	{
		Client client = new Client();
		Vector<String> columns = View.getFieldsOfObject(client);
		
        model = new DefaultTableModel();
        model.setColumnIdentifiers(columns);
        this.setModel(model);
        
        this.setBackground(Color.LIGHT_GRAY);
        this.setForeground(Color.black);
        Font font = new Font("",1,22);
        this.setFont(font);
        this.setRowHeight(30);
	}
	
	public static void addClient(Client client)
	{
		Object[] row = new Object[4];
		row[0] = client.getId();
		row[1] = client.getName();
		row[2] = client.getMoney();
		row[3] = client.getEmail();
		model.addRow(row);
	}
	
}
