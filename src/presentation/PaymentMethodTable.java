package presentation;

import java.awt.Color;
import java.awt.Font;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.PaymentMethod;

public class PaymentMethodTable extends JTable
{
	static DefaultTableModel model;
	
	PaymentMethodTable() throws IllegalArgumentException, IllegalAccessException
	{
		PaymentMethod paymentMethod = new PaymentMethod();
        Vector<String> columns = View.getFieldsOfObject(paymentMethod);
        model = new DefaultTableModel();
        model.setColumnIdentifiers(columns);
        this.setModel(model);
        
        this.setBackground(Color.LIGHT_GRAY);
        this.setForeground(Color.black);
        Font font = new Font("",1,22);
        this.setFont(font);
        this.setRowHeight(30);
	}
	
	public static void addPaymentMethod(PaymentMethod paymentMethod)
	{
		Object[] row = new Object[2];
		row[0] = paymentMethod.getId();
		row[1] = paymentMethod.getPaymentMethod();
		model.addRow(row);	
	}
}
