package presentation;

import javax.swing.JFrame;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import dao.ClientDAO;
import dao.OrderDAO;
import dao.PaymentMethodDAO;
import dao.ProductDAO;
import javafx.scene.text.FontWeight;
import model.*;

public class View
{
	JFrame jframe;
	
	JPanel clientTableLabelPanel;
	JLabel clientTableLabel;
	
	JPanel clientPanel;
	static ClientTable clientTable;
	JScrollPane clientScroll;
	JPanel clientButtonsPanel;
	JButton clientAddButton;
	JButton clientUpdateButton;
	JButton clientDeleteButton;
	
	JPanel clientOperationsPanel;
	JLabel clientIdLabel;
	JLabel clientNameLabel;
	JLabel clientMoneyLabel;
	JLabel clientEmailLabel;
	JTextField clientIdTextField;
	JTextField clientNameTextField;
	JTextField clientMoneyTextField;
	JTextField clientEmailTextField;
	
	JPanel allClientStuffInOnePanel;
	JPanel allProductStuffInOnePanel;
	JPanel allPaymentMethodStuffInOnePanel;
	JPanel allOrderStuffInOnePanel;
	
	JPanel productTableLabelPanel;
	JLabel productTableLabel;
	
	JPanel productPanel;
	static ProductTable productTable;
	JScrollPane productScroll;
	
	JPanel productOperationsPanel;
	JLabel productIdLabel;
	JLabel productNameLabel;
	JLabel productCostLabel;
	JLabel productStockLabel;
	JTextField productIdTextField;
	JTextField productNameTextField;
	JTextField productCostTextField;
	JTextField productStockTextField;
	
	JPanel productButtonsPanel;
	JButton productAddButton;
	JButton productUpdateButton;
	JButton productDeleteButton;
	
	
	JPanel paymentMethodTableLabelPanel;
	JLabel paymentMethodTableLabel;
	
	JPanel paymentMethodPanel;
	static PaymentMethodTable paymentMethodTable;
	JScrollPane paymentMethodScroll;
	
	JPanel paymentMethodOperationsPanel;
	JLabel paymentMethodIdLabel;
	JLabel paymentMethodLabel;
	JTextField paymentMethodIdTextField;
	JTextField paymentMethodTextField;
	
	JPanel paymentMethodButtonsPanel;
	JButton paymentMethodAddButton;
	JButton paymentMethodUpdateButton;
	JButton paymentMethodDeleteButton;
	
	
	JPanel orderTableLabelPanel;
	JLabel orderTableLabel;
	
	JPanel orderPanel;
	static OrderTable orderTable;
	JScrollPane orderScroll;
	
	JPanel orderOperationsPanel;
	JLabel orderQuantityLabel;
	JTextField orderQuantityTextField;
	
	JPanel orderButtonsPanel;
	JButton orderAddButton;
	JButton orderDeleteButton;
	JButton orderCreateBillButton;
	
	
	JPanel finalPanel;

	View() throws IllegalArgumentException, IllegalAccessException
	{
		jframe = new JFrame("Warehouse Processing");
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.setSize(800, 1000);
		
		clientTableLabelPanel = new JPanel();
		clientTableLabel = new JLabel("Clients");
		clientTableLabel.setFont(new Font("Verdana", 1, 25));
		clientTableLabelPanel.add(clientTableLabel);
		
		clientPanel = new JPanel();
		clientTable = new ClientTable();
		clientScroll = new JScrollPane(clientTable);
		clientPanel.add(clientScroll);

		clientOperationsPanel = new JPanel();
		clientIdLabel = new JLabel("id:");
		clientNameLabel = new JLabel("name:");
		clientMoneyLabel = new JLabel("money:");
		clientEmailLabel = new JLabel("email:");
		clientIdTextField = new JTextField();
		clientNameTextField = new JTextField();
		clientMoneyTextField = new JTextField();
		clientEmailTextField = new JTextField();
		clientIdTextField.setPreferredSize(new Dimension(50, 20));
		clientNameTextField.setPreferredSize(new Dimension(200, 20));
		clientMoneyTextField.setPreferredSize(new Dimension(100, 20));
		clientEmailTextField.setPreferredSize(new Dimension(270, 20));
		clientOperationsPanel.add(clientIdLabel);
		clientOperationsPanel.add(clientIdTextField);
		clientOperationsPanel.add(clientNameLabel);
		clientOperationsPanel.add(clientNameTextField);
		clientOperationsPanel.add(clientMoneyLabel);
		clientOperationsPanel.add(clientMoneyTextField);
		clientOperationsPanel.add(clientEmailLabel);
		clientOperationsPanel.add(clientEmailTextField);
		
		
		
		clientButtonsPanel = new JPanel();
		clientAddButton = new JButton("Add");
		clientAddButton.addActionListener(new AddClientActionListener());
		clientUpdateButton = new JButton("Update");
		clientUpdateButton.addActionListener(new UpdateClientActionListener());
		clientDeleteButton = new JButton("Delete");
		clientDeleteButton.addActionListener(new DeleteClientActionListener());
		clientButtonsPanel.add(clientAddButton);
		clientButtonsPanel.add(clientUpdateButton);
		clientButtonsPanel.add(clientDeleteButton);
		
		
		productTableLabelPanel = new JPanel();
		productTableLabel = new JLabel("Products");
		productTableLabel.setFont(new Font("Verdana", 1, 25));
		productTableLabelPanel.add(productTableLabel);
		
		productPanel = new JPanel();
		productTable = new ProductTable();
		productScroll = new JScrollPane(productTable);
		productPanel.add(productScroll);
		
		productOperationsPanel = new JPanel();
		productIdLabel = new JLabel("id:");
		productNameLabel = new JLabel("name:");
		productCostLabel = new JLabel("cost:");
		productStockLabel = new JLabel("stock:");
		productIdTextField = new JTextField();
		productIdTextField.setPreferredSize(new Dimension(50, 20));
		productNameTextField = new JTextField();
		productNameTextField.setPreferredSize(new Dimension(200, 20));
		productCostTextField = new JTextField();
		productCostTextField.setPreferredSize(new Dimension(50, 20));
		productStockTextField = new JTextField();
		productStockTextField.setPreferredSize(new Dimension(50, 20));
		productOperationsPanel.add(productIdLabel);
		productOperationsPanel.add(productIdTextField);
		productOperationsPanel.add(productNameLabel);
		productOperationsPanel.add(productNameTextField);
		productOperationsPanel.add(productCostLabel);
		productOperationsPanel.add(productCostTextField);
		productOperationsPanel.add(productStockLabel);
		productOperationsPanel.add(productStockTextField);
		productButtonsPanel = new JPanel();
		productAddButton = new JButton("Add");
		productAddButton.addActionListener(new AddProductActionListener());
		productUpdateButton = new JButton("Update");
		productUpdateButton.addActionListener(new UpdateProductActionListener());
		productDeleteButton = new JButton("Delete");
		productDeleteButton.addActionListener(new DeleteProductActionListener());
		productButtonsPanel.add(productAddButton);
		productButtonsPanel.add(productUpdateButton);
		productButtonsPanel.add(productDeleteButton);
		
		
		paymentMethodTableLabelPanel = new JPanel();
		paymentMethodTableLabel = new JLabel("Payment methods");
		paymentMethodTableLabel.setFont(new Font("Verdana", 1, 25));
		paymentMethodTableLabelPanel.add(paymentMethodTableLabel);
		
		paymentMethodPanel = new JPanel();
		paymentMethodTable = new PaymentMethodTable();
		paymentMethodScroll = new JScrollPane(paymentMethodTable);
		paymentMethodPanel.add(paymentMethodScroll);
		
		paymentMethodOperationsPanel = new JPanel();
		paymentMethodIdLabel = new JLabel("id:");
		paymentMethodLabel = new JLabel("method:");
		paymentMethodIdTextField = new JTextField();
		paymentMethodIdTextField.setPreferredSize(new Dimension(100,20));
		paymentMethodTextField = new JTextField();
		paymentMethodTextField.setPreferredSize(new Dimension(300,20));
		paymentMethodOperationsPanel.add(paymentMethodIdLabel);
		paymentMethodOperationsPanel.add(paymentMethodIdTextField);
		paymentMethodOperationsPanel.add(paymentMethodLabel);
		paymentMethodOperationsPanel.add(paymentMethodTextField);
		
		paymentMethodButtonsPanel = new JPanel();
		paymentMethodAddButton = new JButton("Add");
		paymentMethodAddButton.addActionListener(new AddPaymentMethodActionListener());
		paymentMethodUpdateButton = new JButton("Update");
		paymentMethodUpdateButton.addActionListener(new UpdatePaymentMethodActionListener());
		paymentMethodDeleteButton = new JButton("Delete");
		paymentMethodDeleteButton.addActionListener(new DeletePaymentMethodActionListener());
		paymentMethodButtonsPanel.add(paymentMethodAddButton);
		paymentMethodButtonsPanel.add(paymentMethodUpdateButton);
		paymentMethodButtonsPanel.add(paymentMethodDeleteButton);
		
		
		
		
		
		orderTableLabelPanel = new JPanel();
		orderTableLabel = new JLabel("Orders");
		orderTableLabel.setFont(new Font("Verdana", 1, 25));
		orderTableLabelPanel.add(orderTableLabel);
		
		orderPanel = new JPanel();
		orderTable = new OrderTable();
		orderScroll = new JScrollPane(orderTable);
		orderPanel.add(orderScroll);
		
		orderOperationsPanel = new JPanel();
		orderQuantityLabel = new JLabel("quantity:");
		orderQuantityTextField = new JTextField();
		orderQuantityTextField.setPreferredSize(new Dimension(100,20));
		orderOperationsPanel.add(orderQuantityLabel);
		orderOperationsPanel.add(orderQuantityTextField);
		
		orderButtonsPanel = new JPanel();
		orderAddButton = new JButton("Add");
		orderAddButton.addActionListener(new AddOrderActionListener());
		orderDeleteButton = new JButton("Delete");
		orderDeleteButton.addActionListener(new DeleteOrderActionListener());
		
		orderCreateBillButton = new JButton("Create bill");
		orderCreateBillButton.addActionListener(new CreateBillActionListener());
		orderButtonsPanel.add(orderAddButton);
		orderButtonsPanel.add(orderDeleteButton);
		orderButtonsPanel.add(orderCreateBillButton);
		
		
		allClientStuffInOnePanel = new JPanel();
		allClientStuffInOnePanel.add(clientTableLabelPanel);
		allClientStuffInOnePanel.add(clientPanel);
		allClientStuffInOnePanel.add(clientOperationsPanel);
		allClientStuffInOnePanel.add(clientButtonsPanel);
		allClientStuffInOnePanel.setLayout(new BoxLayout(allClientStuffInOnePanel, BoxLayout.PAGE_AXIS));
		allClientStuffInOnePanel.setBorder(BorderFactory.createLineBorder(Color.black));
		
		allProductStuffInOnePanel = new JPanel();
		allProductStuffInOnePanel.add(productTableLabelPanel);
		allProductStuffInOnePanel.add(productPanel);
		allProductStuffInOnePanel.add(productOperationsPanel);
		allProductStuffInOnePanel.add(productButtonsPanel);
		allProductStuffInOnePanel.setLayout(new BoxLayout(allProductStuffInOnePanel, BoxLayout.PAGE_AXIS));
		allProductStuffInOnePanel.setBorder(BorderFactory.createLineBorder(Color.black));

		
		allPaymentMethodStuffInOnePanel = new JPanel();
		allPaymentMethodStuffInOnePanel.add(paymentMethodTableLabelPanel);
		allPaymentMethodStuffInOnePanel.add(paymentMethodPanel);
		allPaymentMethodStuffInOnePanel.add(paymentMethodOperationsPanel);
		allPaymentMethodStuffInOnePanel.add(paymentMethodButtonsPanel);
		allPaymentMethodStuffInOnePanel.setLayout(new BoxLayout(allPaymentMethodStuffInOnePanel, BoxLayout.PAGE_AXIS));
		allPaymentMethodStuffInOnePanel.setBorder(BorderFactory.createLineBorder(Color.black));

		
		allOrderStuffInOnePanel = new JPanel();
		allOrderStuffInOnePanel.add(orderTableLabelPanel);
		allOrderStuffInOnePanel.add(orderPanel);
		allOrderStuffInOnePanel.add(orderOperationsPanel);
		allOrderStuffInOnePanel.add(orderButtonsPanel);
		allOrderStuffInOnePanel.setLayout(new BoxLayout(allOrderStuffInOnePanel, BoxLayout.PAGE_AXIS));
		allOrderStuffInOnePanel.setBorder(BorderFactory.createLineBorder(Color.black));
		
		
		finalPanel = new JPanel();
		finalPanel.add(allClientStuffInOnePanel);
		finalPanel.add(allProductStuffInOnePanel);
		finalPanel.add(allPaymentMethodStuffInOnePanel);
		finalPanel.add(allOrderStuffInOnePanel);
		finalPanel.setLayout(new BoxLayout(finalPanel, BoxLayout.PAGE_AXIS));
		jframe.setResizable(false);
		jframe.add(finalPanel);
		jframe.setContentPane(finalPanel);
		jframe.setVisible(true);
	}
	
	public static void loadDatabaseToGUI()
	{
		loadAllClientsFromDatabaseTo(clientTable);
		loadAllOrdersFromDatabaseTo(orderTable);
		loadAllProductsFromDatabaseTo(productTable);
		loadAllPaymentMethodsFromDatabaseTo(paymentMethodTable);
	}
	
	public static void clearTablesFromGUI()
	{
		removeAllRows(clientTable);		
		removeAllRows(productTable);		
		removeAllRows(paymentMethodTable);		
		removeAllRows(orderTable);	
	}
	public static void startApplication() throws IllegalArgumentException, IllegalAccessException
	{	
		View view = new View();
		loadDatabaseToGUI();
	}
	
	private static void loadAllClientsFromDatabaseTo(ClientTable clientTable)
	{
		ArrayList<Client> allClients = ClientDAO.selectAll();
		for(Client c : allClients)
		{
			clientTable.addClient(c);
		}
	}
	
	private static void loadAllOrdersFromDatabaseTo(OrderTable orderTable)
	{
		ArrayList<Order> allOrders = OrderDAO.selectAll();
		for(Order o : allOrders)
		{
			orderTable.addOrder(o);
		}
	}
	
	private static void loadAllProductsFromDatabaseTo(ProductTable productTable)
	{
		ArrayList<Product> allProducts = ProductDAO.selectAll();
		for(Product p : allProducts)
		{
			productTable.addProduct(p);
		}
	}
	
	private static void loadAllPaymentMethodsFromDatabaseTo(PaymentMethodTable paymentMethodTable)
	{
		ArrayList<PaymentMethod> allPaymentMethods = PaymentMethodDAO.selectAll();
		for(PaymentMethod pm : allPaymentMethods)
		{
			paymentMethodTable.addPaymentMethod(pm);
		}
	}
	
	public static Vector<String> getFieldsOfObject(Object object) throws IllegalArgumentException, IllegalAccessException
	{
		Field[] fields = object.getClass().getDeclaredFields();
		Vector<String> allFields = new Vector<String>();
		for(Field f : fields)
		{	
			f.setAccessible(true);
			allFields.add(f.getName());
		}
		return allFields;
	}
	
	private static void removeAllRows(JTable jtable)
	{
		DefaultTableModel dm = (DefaultTableModel) jtable.getModel();
		int rowCount = dm.getRowCount();
		//Remove rows one by one from the end of the table
		for (int i = rowCount - 1; i >= 0; i--) {
		    dm.removeRow(i);
		}
	}
	
	private class AddClientActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			int id = Integer.parseInt(clientIdTextField.getText());
			String name = clientNameTextField.getText();
			int money = Integer.parseInt(clientMoneyTextField.getText());
			String email = clientEmailTextField.getText();
			Client client = new Client(id, name, money, email);
			ClientDAO.add(client);
			
			clearTablesFromGUI();
			loadDatabaseToGUI();
		}
	}
	
	private class DeleteClientActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			int selectedRow = clientTable.getSelectedRow();
			int idOfDeletedClient = 0;
			idOfDeletedClient = (int) clientTable.model.getValueAt(selectedRow, 0);
			ClientDAO.delete(idOfDeletedClient);
			
			clearTablesFromGUI();
			loadDatabaseToGUI();
		}
	}
	
	private class UpdateClientActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			int selectedRow = clientTable.getSelectedRow();
			int idOfToUpdateClient = 0;
			idOfToUpdateClient = (int) clientTable.model.getValueAt(selectedRow, 0);
			
			if(idOfToUpdateClient != 0)
			{
				Client client = ClientDAO.findById(idOfToUpdateClient);
				String newName = client.getName();
				int newMoney = client.getMoney();
				String newEmail = client.getEmail();
				
				if(!clientNameTextField.getText().trim().equals(""))
				{
					newName = clientNameTextField.getText();
				}
				
				if(!clientMoneyTextField.getText().trim().equals(""))
				{
					newMoney = Integer.parseInt(clientMoneyTextField.getText());
				}
				
				if(!clientEmailTextField.getText().trim().equals(""))
				{
					newEmail = clientEmailTextField.getText();
				}
				
				client.setName(newName);
				client.setMoney(newMoney);
				client.setEmail(newEmail);
				ClientDAO.update(client);
				
				clearTablesFromGUI();
				loadDatabaseToGUI();
			}
		}
	}
	
	private class AddProductActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			int id = Integer.parseInt(productIdTextField.getText());
			String name = productNameTextField.getText();
			int cost = Integer.parseInt(productCostTextField.getText());
			int stock = Integer.parseInt(productStockTextField.getText());
			Product product = new Product(id, name, cost, stock);
			ProductDAO.add(product);
			
			clearTablesFromGUI();
			loadDatabaseToGUI();
		}
	}
	
	private class DeleteProductActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			int selectedRow = productTable.getSelectedRow();
			int idOfDeletedProduct = 0;
			idOfDeletedProduct = (int) productTable.model.getValueAt(selectedRow, 0);
			ProductDAO.delete(idOfDeletedProduct);
			
			clearTablesFromGUI();
			loadDatabaseToGUI();
		}
	}
	
	private class UpdateProductActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			int selectedRow = productTable.getSelectedRow();
			int idOfToUpdateProduct = 0;
			idOfToUpdateProduct = (int) productTable.model.getValueAt(selectedRow, 0);
			
			if(idOfToUpdateProduct != 0)
			{
				Product product = ProductDAO.findById(idOfToUpdateProduct);
				String newName = product.getName();
				int newCost = product.getCost();
				int newStock = product.getStock();
				
				if(!productNameTextField.getText().trim().equals(""))
				{
					newName = productNameTextField.getText();
				}
				
				if(!productCostTextField.getText().trim().equals(""))
				{
					newCost = Integer.parseInt(productCostTextField.getText());
				}
				
				if(!productCostTextField.getText().trim().equals(""))
				{
					newStock = Integer.parseInt(productStockTextField.getText());
				}
				
				product.setName(newName);
				product.setCost(newCost);
				product.setStock(newStock);
				ProductDAO.update(product);
				
				clearTablesFromGUI();
				loadDatabaseToGUI();;
			}
		}
	}
	
	private class AddPaymentMethodActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			int id = Integer.parseInt(paymentMethodIdTextField.getText());
			String method = paymentMethodTextField.getText();

			PaymentMethod paymentMethod = new PaymentMethod(id, method);
			PaymentMethodDAO.add(paymentMethod);
			
			clearTablesFromGUI();
			loadDatabaseToGUI();
		}
	}
	
	
	private class DeletePaymentMethodActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			int selectedRow = paymentMethodTable.getSelectedRow();
			int idOfDeletedPaymentMethod = 0;
			idOfDeletedPaymentMethod = (int) paymentMethodTable.model.getValueAt(selectedRow, 0);
			PaymentMethodDAO.delete(idOfDeletedPaymentMethod);
			
			clearTablesFromGUI();
			loadDatabaseToGUI();
		}
	}
	
	private class UpdatePaymentMethodActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			int selectedRow = paymentMethodTable.getSelectedRow();
			int idOfToUpdatePaymentMethod = 0;
			idOfToUpdatePaymentMethod = (int) paymentMethodTable.model.getValueAt(selectedRow, 0);
			
			if(idOfToUpdatePaymentMethod != 0)
			{
				PaymentMethod paymentMethod = PaymentMethodDAO.findById(idOfToUpdatePaymentMethod);
				String newMethod = paymentMethod.getPaymentMethod();
				
				if(!paymentMethodTextField.getText().trim().equals(""))
				{
					newMethod = paymentMethodTextField.getText();
				}
				
				paymentMethod.setPaymentMethod(newMethod);
				PaymentMethodDAO.update(paymentMethod);
				
				clearTablesFromGUI();
				loadDatabaseToGUI();
			}
		}
	}
	
	private class AddOrderActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			int selectedRowOfClientTable = clientTable.getSelectedRow();
			int idOfSelectedClient = (int) clientTable.model.getValueAt(selectedRowOfClientTable, 0);
			Client client = ClientDAO.findById(idOfSelectedClient);
			
			int selectedRowOfProductTable = productTable.getSelectedRow();
			int idOfSelectedProduct = (int) productTable.model.getValueAt(selectedRowOfProductTable, 0);
			Product product = ProductDAO.findById(idOfSelectedProduct);
			
			int selectedRowOfPaymentMethodTable = paymentMethodTable.getSelectedRow();
			int idOfSelectedPaymentMethod = (int) paymentMethodTable.model.getValueAt(selectedRowOfPaymentMethodTable, 0);
			PaymentMethod paymentMethod = PaymentMethodDAO.findById(idOfSelectedPaymentMethod);
			
			int quantity = Integer.parseInt(orderQuantityTextField.getText());
			
			OrderDAO.add(client, product, paymentMethod, quantity);
			
			clearTablesFromGUI();
			loadDatabaseToGUI();
		}
	}
	
	private class DeleteOrderActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			int selectedRow = orderTable.getSelectedRow();
			int idOfDeletedOrder = (int) orderTable.model.getValueAt(selectedRow, 0);
			
			//Update stock of product
			int productId = (int) orderTable.model.getValueAt(selectedRow, 2);
			Product product = ProductDAO.findById(productId);
			int originalStock = product.getStock();
			int deletedStock = OrderDAO.findById(idOfDeletedOrder).getQuantity();
			product.setStock(originalStock + deletedStock);
			ProductDAO.update(product);
			
			OrderDAO.delete(idOfDeletedOrder);
			
			clearTablesFromGUI();
			loadDatabaseToGUI();
		}
	}
	
	private class CreateBillActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			try
			{
				int selectedRow = orderTable.getSelectedRow();
				int idOfSelectedOrder = (int) orderTable.model.getValueAt(selectedRow, 0);
				Order selectedOrder = OrderDAO.findById(idOfSelectedOrder);
				
				int idOfclientOfSelectedOrder = selectedOrder.getClientId();
				Client clientOfSelectedOrder = ClientDAO.findById(idOfclientOfSelectedOrder);
				
				int idOfProductOfSelectedOrder = selectedOrder.getProductId();
				Product productOfSelectedOrder = ProductDAO.findById(idOfProductOfSelectedOrder);
				
				int idOfPaymentMethodOfSelectedOrder = selectedOrder.getPaymentId();
				PaymentMethod paymentMethodOfSelectedOrder = PaymentMethodDAO.findById(idOfPaymentMethodOfSelectedOrder);
				
				try
				{
					FileWriter fileWriter = new FileWriter("bill.txt");
				    PrintWriter printWriter = new PrintWriter(fileWriter);
					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
					LocalDateTime now = LocalDateTime.now();
					printWriter.println("--Bill created at " + dtf.format(now));
					printWriter.println("Order ID: " + selectedOrder.getId());
					printWriter.println("Product: " + productOfSelectedOrder.getName());
					printWriter.println("Quantity bought: " + selectedOrder.getQuantity());
					printWriter.println("Client: " + clientOfSelectedOrder.getName());
					printWriter.println("Payment method: " + paymentMethodOfSelectedOrder.getPaymentMethod());
					printWriter.close();
					JOptionPane.showMessageDialog(null, "Bill succesfully created!", "Michael Scott", JOptionPane.INFORMATION_MESSAGE);
				} 
				catch (IOException e)
				{
					e.printStackTrace();
				}
			
			}
			catch(ArrayIndexOutOfBoundsException e)
			{
				JOptionPane.showMessageDialog(null, "You need to select an order first.", "Error", JOptionPane.WARNING_MESSAGE);
			}
		}
	}
	
}
