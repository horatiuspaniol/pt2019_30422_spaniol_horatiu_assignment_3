package presentation;
import java.awt.Color;
import java.awt.Font;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Order;

public class OrderTable extends JTable
{
	static DefaultTableModel model;
	
	OrderTable() throws IllegalArgumentException, IllegalAccessException
	{
		Order order = new Order();
        Vector<String> columns = View.getFieldsOfObject(order);
        model = new DefaultTableModel();
        model.setColumnIdentifiers(columns);
        this.setModel(model);
        
        this.setBackground(Color.LIGHT_GRAY);
        this.setForeground(Color.black);
        Font font = new Font("",1,22);
        this.setFont(font);
        this.setRowHeight(30);
	}
	
	public static void addOrder(Order order)
	{
		Object[] row = new Object[5];
		row[0] = order.getId();
		row[1] = order.getClientId();
		row[2] = order.getProductId();
		row[3] = order.getPaymentId();
		row[4] = order.getQuantity();
		model.addRow(row);
	}
}
