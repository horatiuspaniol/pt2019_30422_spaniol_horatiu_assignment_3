package model;

public class Product
{
	private int id;
	private String name;
	private int cost;
	private int stock;
	
	public Product(int id, String name, int cost, int stock)
	{
		super();
		this.id = id;
		this.name = name;
		this.cost = cost;
		this.stock = stock;
	}
	
	public Product(String name, int cost, int stock)
	{
		super();
		this.name = name;
		this.cost = cost;
		this.stock = stock;
	}
	
	public Product()
	{

	}

	public int getId()
	{
		return this.id;
	}

	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public int getCost()
	{
		return this.cost;
	}
	
	public void setCost(int cost)
	{
		this.cost = cost;
	}
	
	public int getStock()
	{
		return this.stock;
	}
	
	public void setStock(int newStock)
	{
		this.stock = newStock;
	}
	
	public String toString()
	{
		return "Product[" + this.id + "] -- Name:" + this.name + " -- Cost:" + this.cost + " -- Stock:" + this.stock;
	}
}


