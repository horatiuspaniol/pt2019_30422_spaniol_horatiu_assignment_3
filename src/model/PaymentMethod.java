package model;

public class PaymentMethod
{
	private int id;
	private String paymentMethod;
	
	public PaymentMethod(String paymentMethod)
	{
		super();
		this.paymentMethod = paymentMethod;
	}
	
	public PaymentMethod(int id, String paymentMethod)
	{
		super();
		this.id = id;
		this.paymentMethod = paymentMethod;
	}
	
	public PaymentMethod()
	{
	
	}

	public int getId()
	{
		return this.id;
	}
	
	public String getPaymentMethod()
	{
		return this.paymentMethod;
	}

	public void setId(int id)
	{
		this.id = id;
	}
	
	public void setPaymentMethod(String paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}
}



