package model;

public class Order
{
	private int id;
	private int clientId;
	private int productId;
	private int paymentId;
	private int quantity;
	
	public Order(int id, int clientId, int productId,int paymentId,  int quantity)
	{
		super();
		this.id = id;
		this.clientId = clientId;
		this.productId = productId;
		this.paymentId = paymentId;
		this.quantity = quantity;
	}
	
	public Order(int id, Client client, Product product, int paymentId,  int quantity)
	{
		super();
		this.id = id;
		this.clientId = client.getId();
		this.productId = product.getId();
		this.quantity = quantity;
		this.paymentId = paymentId;
	}
	
	public Order(Client client, Product product, PaymentMethod paymentMethod,  int quantity)
	{
		super();
		this.clientId = client.getId();
		this.productId = product.getId();
		this.quantity = quantity;
		this.paymentId = paymentMethod.getId();
	}
	
	public Order(int clientId, int productId,int quantity,  int paymentId)
	{
		super();
		this.clientId = clientId;
		this.productId = productId;
		this.quantity = quantity;
		this.paymentId = paymentId;
	}
	
	public Order()
	{
		
	}

	public int getId()
	{
		return this.id;
	}
	
	public int getClientId()
	{
		return this.clientId;
	}
	
	public int getProductId()
	{
		return this.productId;
	}
	
	public int getQuantity()
	{
		return this.quantity;
	}
	
	public int getPaymentId()
	{
		return this.paymentId;
	}	
}
