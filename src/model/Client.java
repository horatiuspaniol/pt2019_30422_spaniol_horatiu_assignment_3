package model;

public class Client extends Object
{
	private int id;
	private String name;
	private int money;
	private String email;
	
	public Client(int id, String name, int money, String email)
	{
		super();
		this.id = id;
		this.name = name;
		this.money = money;
		this.email = email;
	}
	
	public Client(String name, int money, String email)
	{
		super();
		this.name = name;
		this.money = money;
		this.email = email;
	}
	
	public Client()
	{
		
	}

	public int getId()
	{
		return this.id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public int getMoney()
	{
		return this.money;
	}
	
	public void setMoney(int money)
	{
		this.money = money;
	}
	
	public String getEmail()
	{
		return this.email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

}
